var MintToken = artifacts.require("MintToken");

contract('MintToken', async function(accounts) {
  var eth = web3.eth;

  var token, account;

  before( async function() {
    token = await MintToken.deployed();
    account = accounts[0];
  });

  it("should have token details set", async function() {
    assert.equal(await token.name(), 'Public Mint');
    assert.equal(await token.symbol(), 'MINT');
    assert.equal(await token.decimals(), 18);
    assert.equal(await token.totalSupply(), 250 * (10**6) * (10**18));
  });

  it("should be able to transfer", async function() {
    await token.transfer(accounts[1], 1000);
    assert.equal(await token.balanceOf(accounts[1]), 1000);
    assert.equal(await token.balanceOf(accounts[0]), (await token.totalSupply()) - 1000);
  });

  it("should be able to transferMany", async function() {
    await token.transferMany([ accounts[2], accounts[3] ], [ 200, 300 ]);
    assert.equal(await token.balanceOf(accounts[2]), 200);
    assert.equal(await token.balanceOf(accounts[3]), 300);
    assert.equal(await token.balanceOf(accounts[0]), (await token.totalSupply()) - 1500);
  });
});
