# Public Mint - MINT Token

This is the source code for MINT - the ERC20 Token suppporting Public Mint on the Ethereum mainnet.

For more information, see [publicmint.com](https://publicmint.com).
